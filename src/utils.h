#ifndef _UTILS
#define _UTILS

//#include "testApp.h"
#include "ofxOpenCv.h"
#include "stdio.h"
#include "math.h"

typedef struct {
  ofPoint pt;
  bool 	bBeingDragged;
  bool 	bOver;
  float 	radius;
}draggableVertex;

typedef struct {
  int  pos_x;
  int  pos_y;
  float ratio_x;
  float ratio_y;
  int height;
  int width;

}monitorRef;

float dist(ofPoint pt1, ofPoint pt2);

void hsv2rgb(int h, int & r,int & g, int & b );
void cvBlackToAlpha(ofxCvGrayscaleImage & gray_image, ofTexture & rgba_image);
void cvGrayToRgba(ofxCvGrayscaleImage & gray_image, ofTexture & rgba_image, int r, int g, int b, bool noir);
void maxImagePix(ofPixels & image_base, ofPixels & image_plus, int type_image);
void minImagePix(ofPixels & image_base, ofPixels & image_plus, int type_image);
void addImage(ofxCvGrayscaleImage & image_base, ofxCvGrayscaleImage & image_plus, int mode);


void addColorImage(ofImage & image_base, ofxCvGrayscaleImage & image_plus, int color);
void setMasque(ofImage & masque, 
	       ofxCvGrayscaleImage & gray_image, 
	       ofTexture & rgba_image);
//void insertImage(ofxCvGrayscaleImage & image, int pos_x, int pos_y, int width, int height);
ofPoint changeRef(ofPoint xy, monitorRef ref);
void gaussian_elimination(float *input, int n);
void findHomography(ofPoint src[4], ofPoint dst[4], float homography[16]);
#endif
