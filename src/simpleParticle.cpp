#include "simpleParticle.h"

SimpleParticle::SimpleParticle(ofPoint position, ofPoint vitesse, int size_in){
  pos = position;
  vit = vitesse;

  size = size_in;
  color.set(255,255,255);

  lifespan = 10000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;
}

void SimpleParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;
  pos += vit * step_duration / 1000.0;
  
  bAlive = isAlive();
}

void SimpleParticle::draw(){
  ofSetColor(color);
  ofFill();
  ofCircle(pos.x, pos.y, size);
  ofNoFill();
  ofSetColor(color*0.5);
  //ofSetColor(0,0,0);
  ofCircle(pos.x, pos.y, size);
  ofSetColor(255,255,255);
}



bool SimpleParticle::isAlive(){
  if (age < lifespan){
    return true;
  }else{
    cout << "age " << age << " lifespan " << lifespan << endl;
    return false;
  }
}

ofPoint SimpleParticle::getPosition(){
  return pos;
}

ofPoint SimpleParticle::getVit(){
  return vit;
}

void SimpleParticle::setColor(int r, int g, int b, int alpha){
  color.set(r, g, b, alpha);
}


SimpleParticle::~SimpleParticle(){
  
}
