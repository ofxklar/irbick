#pragma once

#include "ofMain.h"
//#include "ofxControlPanel.h"
//#include "ofxUI.h"
#include "ofxGui.h"
#include "ofxOsc.h"
/* #include "ofxVideoRecorder.h" */

#include "LoopRecorder.h"
#include "nakedStairs.h"
#include "delayParticles.h"
#include "triangleLocation.h"

// listen on port 12345
//#define PORT 23456
#define PORT 8000
#define OUTPORT 339988
#define HOST "localhost"

#define NUM_MSG_STRINGS 20
#define N_DRAG_POINTS 15;

class FrameData{
 public:
  ofPoint pt[3];
};

class testApp : public ofBaseApp{

 public:
  void setup();
  void setupGUI();

  void update();
  void updateOsc();

  void draw();

  void exit();
  
  void keyPressed  (int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);
  /* void guiEvent(ofxUIEventArgs &e); */
  void coordChanged(float & x);
  void radiusChanged(float & x);
  
  void drawInput(monitorRef monitor);
  void drawKaleidoscope();
  void drawKaleidoscopeBis();
  void drawMandala();
  void drawAngle();
  void drawTriangle(ofPoint tex_coord[3], ofPoint triangle_coord[3]);
  void drawTriangleDel(ofPoint tex_coord[3], ofPoint triangle_coord[3]);
  void drawTriangleDel2(ofPoint tex_coord[3], ofPoint triangle_coord[3], int del);
  void drawAngleDelay();
  void drawTriangleDelay(ofPoint tex_coord[3], ofPoint triangle_coord[3]);

  void updateLp();

  void setTexCoord(int radius, int angle);

  void grabNote();

  void drawNote();
  DelayParticles delay_particles;

  ofxPanel gui;
  ofParameter<bool> draw_gui;
  ofParameter<bool> draw_input;

  ofParameter<float> n_delay;

  bool mode_kaleidoscope;
  bool mode_mandala;
  bool mode_full;

  ofPoint center;

  ofImage mire;
  ofImage im_screen;
  ofImage im_lp;

  int width;
  int height;
  ofTexture input_tex;
  ofTexture output_tex;
  ofPoint triangle_coord[3][3];
  ofPoint tex_coord[3];
  ofVideoGrabber  input_vid;
  ofPixels pix;

  ofParameter<float> radius_in;
  ofParameter<float> radius_1;
  ofParameter<float> angle_in;

  int liste_note[100];

  monitorRef monitor;
  int heigth_m;
  int width_m;

  LoopRecorder vid_buffer;
  NakedStairs delay;

  LoopRecorder lp_buffer;
  NakedStairs lp_delay;
  int lp_n_delay;
  int lp_len_delay;
  int lp_decrease;
  bool lp_mode_black;
  bool lp_rgb;
  int lp_n_chan;

  float cpt_delay;
  int mode_delay_bis;

  string rosace_input;


  float map_b, map_w;

  draggableVertex drag_points[15] ;
  int n_drag_points;

  /* ofxUICanvas *gui; */
  float elapsedTime;
  int appFrameCount;
  float appFrameRate;

  int angle_rotation;
  int n_angles;
  int mode_free;
  int n_triangles;
  float angle;
  int radius_2, radius_3, radius_4;

  ofxOscReceiver receiver;
  int current_msg_string;
  string msg_strings[NUM_MSG_STRINGS];
  float timers[NUM_MSG_STRINGS];

  TriangleLocation tr_location;
  int n_triangles_k;
  int n_triangles_tot[10];

  ofxOscSender sender;
  bool mode_grab;
  ofImage grab;
  bool sender_is_setup;
  int time_to_turn;
  int grab_angle;

  int grab_radius_1;
  int grab_radius_2;

  bool mode_audio ;
  bool draw_grab;
  bool draw_audio ;

  void audioOut(float * input, int bufferSize, int nChannels);
  void drawAudio();
  void drawGrab();

  ofSoundStream soundStream;
  
  float 	pan;
  int		sampleRate;

  float step_by_tour;

  float volume_angle;
  ofPoint volume_pos;

  float 	volume;

  vector <float> lAudio;
  vector <float> rAudio;

};
