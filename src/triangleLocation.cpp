#include "triangleLocation.h"

bool sortDist(triangleElem i,triangleElem j) { return (i.dist < j.dist); }

TriangleLocation::TriangleLocation(){
  tr_width = 0;
  center = ofPoint(ofGetWidth()/2., ofGetHeight()/2.);
}

void TriangleLocation::update(){
  generateKaleidoscope();
  sortDistFromPoint();
}

void TriangleLocation::setTriangleWidth(float width){
  tr_width = width;
  update();
}

void TriangleLocation::addTriangle(){
}

void TriangleLocation::generateKaleidoscope(){
  int width = ofGetWidth();
  int height = ofGetHeight();
  
  float tr_height = tr_width * sqrt(3)/2.0;

  int n_x = 0;
  int n_y = 0;

  float x = 0;
  float y = 0;



  float x0 = 0;

  int n_xx = max(floor(width/(tr_width*3))*1.0,1.0) * 2;

  x0 = -(n_xx  * tr_width*3 - width) / 2;

  //float x0 = ((width/(tr_width*3)) - floor(width/(tr_width*3))) * tr_width*3/2 ;
  //y = - ((width/tr_height) - ceil(width/tr_height)) * tr_height /2;

  int n_yy = max(floor(height/(tr_height*2))*1.0,1.0) * 2;

  y = -(n_yy  * tr_height*2 - height) / 2;

  x = x0;

  cout << "y " << y << " || tr_height " << tr_height << "  || n_yy " << n_yy << endl;

  triangleList.clear();

  while (y < height + tr_height){
    while (x < width + tr_width){
      generateHex(x, y);
      x += 3 * tr_width;
      n_x ++;
    }
    y += tr_height;
    n_y ++;
    if (n_y%2){
      x = x0 + 1.5 * tr_width ;
    }else{
      x = x0; 
    }
  }
}

void TriangleLocation::generateHex(float x, float y){
  for (int i = 0; i < 6; i++){
    ofPoint pts[3];
    int angle = i * 60;

    pts[0].set(x,y);        
    if (i%2){
      pts[1].set(x + cos(angle*PI/180)*tr_width, y + sin(angle*PI/180)*tr_width);
      angle += 60;
      pts[2].set(x + cos(angle*PI/180)*tr_width, y + sin(angle*PI/180)*tr_width);
    }else{
      pts[2].set(x + cos(angle*PI/180)*tr_width, y + sin(angle*PI/180)*tr_width);
      angle += 60;
      pts[1].set(x + cos(angle*PI/180)*tr_width, y + sin(angle*PI/180)*tr_width);
    }
    
    triangleElem tr;
    tr.pts[0] = pts[0];
    tr.pts[1] = pts[1];
    tr.pts[2] = pts[2];
    tr.visible = true;
    tr.index = triangleList.size();
    tr.delay = triangleList.size()/12;
    triangleList.push_back(tr);
  }
}

void TriangleLocation::getCenters(){
  for (int i = 0 ; i<triangleList.size() ; i++){
    float x = 0;
    float y = 0;
    for (int j = 0; j<3; j++){
      x += triangleList[i].pts[j].x / 3;
      y += triangleList[i].pts[j].y / 3;
    }
    triangleList[i].center.set(x,y);
  }
}

void TriangleLocation::getDistFromPoint(){
  getCenters();
  for (int i = 0 ; i<triangleList.size() ; i++){
    float dist_val =  dist(triangleList[i].center, center);
    triangleList[i].dist = dist_val;
  } 
}

void TriangleLocation::sortDistFromPoint(){
  getDistFromPoint();
  std::sort(triangleList.begin(), triangleList.end(), sortDist);
  for (int i = 0 ; i<triangleList.size() ; i++){
    triangleList[i].delay = i/12.;
  }
}

void TriangleLocation::setCenter(ofPoint center_in){
  center = center_in;
  sortDistFromPoint();
}


triangleElem TriangleLocation::getTriangleElem(int i){
  return triangleList[i];
}

int TriangleLocation::getLength(){
  return triangleList.size();
}

TriangleLocation::~TriangleLocation(){
}
