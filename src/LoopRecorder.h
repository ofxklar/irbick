#ifndef LOOPRECORDER_H
#define LOOPRECORDER_H

#include "ofxOpenCv.h"

#define N_FRAME_MAX 20000

class LoopRecorder
{
public:
	LoopRecorder();
	virtual ~LoopRecorder();

	int allocate(int width, int height, int lenght_in, int pixel_type);

	void    setRecordMode();
	void    setRecordModeOn();
	void    setRecordModeOff();
	int     getRecordMode();
	void    play();
	void    stop();
	void    setPlayMode();
	void    setRestart();
	int     getPlayMode();
	void    getLoopEnd();
	void    setLenghtMaster(int lenght_master);
	bool    getDelayOn();
	void    setLatence(int lat);

	int     grabFrame(ofPixels &);
	void    setData(int i_data, void * data_in);
	void*   getData(int i_data);
	int     grabFrameDepth(ofPixels &, int depth);
	void    setFrame(float pos);
	ofPixels getFrame();
	ofPixels getFrameN(int n);
	ofPixels getFrameDelay(int delays_ts);
	ofPixels getCurrentFramePlay();

	int    getDepthN(int n);
	int    getCurrentFrameRecord();
	int    getCurrentFrameNum();

	int    getPosition();
	int    getLoopLenght();
	int    getLoopLenghtMillis();
	int    getLenghtMax();
	float  getCurrentLenghtRecord();
	void   setLenghtMillis(float lenght_loop_ms);
	int    length_max;

	int    width;
	int    height;
	int    pixel_type;
	int    loop_length;

protected:

private:

	int    i_record;
	int    i_play;


	bool   record_mode;
	bool   play_mode;

	ofPixels    frames[N_FRAME_MAX];
	int    timestamp[N_FRAME_MAX];
	int    depth_record[N_FRAME_MAX];

	int    start_ms;
	float  length_ms;
	int    start_loop_ms;
	int    length_loop_ms;
	int    start_play_ms;
	float  length_current_ms;
	float  t_trigered_ms;

	int    length_play_ms;
	int    length_master_ms;

	int    delai;
	int    compteur_delai;

	int    latence;

	void *data[N_FRAME_MAX];
};

#endif // OFXVIDEOLOOP_H
