#include "nakedStairs.h"

NakedStairs::NakedStairs()
{
}

void NakedStairs::init(int ns_nvids_in,
		int lenght_in,
		int width,
		int height,
		LoopRecorder & recorder_in){
  buffer_lenght = lenght_in;
  input_width = width;
  input_height = height;
  this->recorder = recorder_in;

  ns_pos_x_init = 100;
  ns_nvids = ns_nvids_in;
  n_naked_stairs = 0;
  naked_stairs_delai = 1;
  ns_old_delai = 0;
  ns_new_delai = 0;
  for (int i =0; i<100; i++){
    ns_lst_delai[i]=0;
  }
  n_naked_stairs = 0;

  // for (int i =0; i<ns_nvids; i++){
  //   ns_gray[i].allocate(input_width, input_height);
  //   ns_rgba[i].allocate(input_width, input_height, GL_RGBA);
  //   ns_gray[i].setUseTexture(false);
  // }

  for (int i=0; i<6; i++){
    ns_transition[i] = 0;
  }
}

void NakedStairs::update(){

  n_naked_stairs = ns_nvids;
  for (int i = 0; i < n_naked_stairs; i++){
    if (ns_lst_delai[i] < ns_new_delai*i){
      ns_lst_delai[i] += 1;
    }
    if (ns_lst_delai[i] > ns_new_delai*i){
      ns_lst_delai[i] -= 1;
    }
  }
}

void NakedStairs::setPosX(int pos_x_in){
  ns_pos_x_init = pos_x_in;
}

void NakedStairs::setTransition(int i , int pos){
  ns_transition[i] = pos;
}

void NakedStairs::setNsnum(int n){
  if (n < 0){
    ns_num = 0; 
  } else if (n*n > ns_nvids){
    ns_num = sqrt(ns_nvids);
  } else {
    ns_num = n;
  }
}

void NakedStairs::increaseNsnum(){
  ns_num ++;
  if (ns_num*ns_num>ns_nvids){
    ns_num --;
  }
}

void NakedStairs::decreaseNsnum(){
  ns_num --;
  if (ns_num < 0){
    ns_num =0;
  }
}

void NakedStairs::setDelay(float n){
  if (n < 0){
    ns_new_delai = 0;
  } else {
    ns_new_delai = n;
  }
}

void NakedStairs::increaseDelay(){
  ns_new_delai ++;
}

void NakedStairs::decreaseDelay(){
  ns_new_delai --;
  if (ns_new_delai < 0){
    ns_new_delai = 0;
  }
}

int NakedStairs::getFramePos(int i, LoopRecorder & record){

	int i_frame = record.getCurrentFrameRecord()-ns_lst_delai[i]-1;
	//cout << "i " << i << " ns " << ns_lst_delai[i] << " i_frame " << i_frame << endl;
	    while (i_frame<0){
	      i_frame = buffer_lenght + i_frame;
	    }

	    if (i_frame==0){
	      i_frame = 0;
	    }
	 return i_frame;
}

void NakedStairs::draw(int x, int y, int w,  int h){
  
  int pos_width = 0;
  int pos_height = 0;
  float n_row = ns_num;
  float n_column = ns_num;
  
  int n_naked_stairs = ns_num*ns_num;
  int vids_height = h / n_row;
  int vids_width = w / n_row;
  
  ns_width = vids_width;
  ns_height = vids_height;
  int sum_delai = 0;
  for (int i = 0; i < n_naked_stairs; i++){
    
    //int i_frame = getFramePos(i);
    
    if (pos_width > n_column-1){
      pos_width = 0;
      pos_height += 1;
    }
    
    if (ns_pos_x_init == 100){
    
//      recorder.getFrameN(i_frame).draw(x + pos_width*vids_width,
//      			       y + pos_height*vids_height,
//      			       vids_width,
//      			       vids_height);
    
    }else{
    
//      ns_gray[i] = recorder.getFrameN(i_frame);
//      cvGrayToRgba(ns_gray[i], ns_rgba[i], 1, 1, 1, 1);
//
//      int ns_pos_x = 0;
//      ns_pos_x = ofMap(ns_transition[pos_height],
//		       0,
//		       100,
//		       -vids_width,
//		       x + pos_width*vids_width * ns_pos_x_init/100.  );
//
//      int ns_pos_y = pos_height*vids_height;
//      ns_rgba[i].draw(ns_pos_x,
//      		      y + ns_pos_y,
//      		      vids_width,
//      		      vids_height);
    }
    pos_width ++;
  }
}

NakedStairs::~NakedStairs()
{

}
