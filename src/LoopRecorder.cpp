#include "LoopRecorder.h"

LoopRecorder::LoopRecorder(){

//	width = 0;
//	height = 0;
//	pixel_type = 0;

    i_record=0;
    i_play=0;

//    start_ms = 0;
//    length_max = 0;
//    delai = 0;
//    compteur_delai = 0;
//    loop_length = 0;
//
//    start_loop_ms = 0;
//    length_ms = 0;
//    start_play_ms = 0;
//    length_master_ms = 0;
//    length_loop_ms = 0;
//    length_play_ms = 0;
//    length_current_ms = 0;
//    t_trigered_ms = 0;
//    latence = 0;
    record_mode = false;
    play_mode = false;}

int LoopRecorder::allocate(int width_in, int height_in, int lenght_in, int pixel_type_in)
{
    width = width_in;
    height = height_in;
    length_max = lenght_in;
    pixel_type = pixel_type_in;
    start_ms = 0;
    length_ms = 0;
    length_current_ms = 0;
    t_trigered_ms = 0;
    delai = 15;
    compteur_delai = delai;

    latence = 0;

    if (length_max > N_FRAME_MAX){
        cout << length_max << " nombre de frame > nb max = " << N_FRAME_MAX;
        length_max = N_FRAME_MAX;
    }

    for (int i = 0; i < length_max; i++){
        //frames[i].setUseTexture(false);
        frames[i].allocate(width, height, pixel_type);
    }
    return 1;
}

void LoopRecorder::setRecordMode()
{
  if (record_mode){
    setRecordModeOff();
  }else{
    setRecordModeOn();
    
  }
}

void LoopRecorder::setRecordModeOn()
{
  record_mode = true;
  start_ms = ofGetElapsedTimeMillis();
  compteur_delai = 0;
  if (loop_length>0){
    length_ms = 0;
    loop_length = 0;
    i_record = 0;
    i_play = 0;
  }
}

void LoopRecorder::setLatence(int lat){
  latence = lat;
}

void LoopRecorder::setLenghtMillis(float loop_lenght_ms){
  length_ms = loop_lenght_ms;
}

void LoopRecorder::setRecordModeOff()
{
  cout << "STOP " << endl;
  record_mode = false;
  compteur_delai = 0;
 length_ms = ofGetElapsedTimeMillis() - start_ms;
 cout << "setRecordModeOff " << length_ms << endl;
}

int LoopRecorder::getRecordMode()
{
    return record_mode;
}

void LoopRecorder::play()
{
  if (loop_length > i_play){
    play_mode = true;
    t_trigered_ms = ofGetElapsedTimeMillis();
    //cout << "play() t_trigered_ms " << t_trigered_ms << " i_play " << endl; 
  }
}

void LoopRecorder::stop()
{
    play_mode = false;
}

void LoopRecorder::setPlayMode(){
  if (play_mode){
    stop();
  }else{
    play();
  }
}

int LoopRecorder::getPlayMode(){
  return play_mode;
}

bool LoopRecorder::getDelayOn(){
  if (compteur_delai < delai){
    //cout << "getDelayOn() true c_delai " << compteur_delai << endl;
    return true;
  }else{
    //cout << "getDelayOn() false "<< compteur_delai << endl;
    return false;
  }
}

int LoopRecorder::grabFrame(ofPixels &frame)
{
  int i_record_bak;
  i_record_bak = i_record;
  if (!record_mode and compteur_delai < delai){
    compteur_delai ++;    
  }
  
  if (record_mode or compteur_delai < delai){
    if (i_record >= length_max){
      i_record = 0;
      frames[i_record] = frame;
      timestamp[i_record] = ofGetElapsedTimeMillis();
      return i_record_bak;
      
    }else{
      frames[i_record] = frame;
      timestamp[i_record] = ofGetElapsedTimeMillis();
      i_record++;
      if (loop_length < length_max){
	loop_length++;
      }
      return i_record_bak;
    }
  }
  else{
    return -1;
  }
}

void LoopRecorder::setData(int i_data, void * data_in){
  data[i_data] = data_in;
}

void* LoopRecorder::getData(int i_data){
  return data[i_data];
}

void LoopRecorder::setFrame(float pos){
  if (loop_length>0){
    i_play = ofMap(pos, 0, 1, 0, loop_length-1);
    length_current_ms = ofMap(pos,0 ,1, 0, length_ms);
    t_trigered_ms = ofGetElapsedTimeMillis();
  }
}

void LoopRecorder::setLenghtMaster(int lenght_master){
  length_master_ms = lenght_master;
}

int LoopRecorder::getLenghtMax(){
  return length_max;
}

int LoopRecorder::getLoopLenghtMillis(){
  return length_ms;
}

int LoopRecorder::getPosition(){
  return i_play;
}

int LoopRecorder::getLoopLenght(){
  return loop_length;
}

void LoopRecorder::setRestart(){

  float tt = (ofGetElapsedTimeMillis() - t_trigered_ms) / length_ms;
  //cout << "setRestart() t_trigered_ms "<< t_trigered_ms << " tt " << tt <<  endl;
  if (tt > 1){
    t_trigered_ms += floor(tt) * length_ms;
    
    
    i_play = 0;
    //cout << "RESTART " << endl;
    //cout << "setRestart() t_trigered_ms " << t_trigered_ms << endl;
  }
  // start_loop_ms = ofGetElapsedTimeMillis();
  // i_play = 0;
}

void LoopRecorder::getLoopEnd(){
  length_current_ms = ofGetElapsedTimeMillis() - t_trigered_ms;
  if (play_mode && !record_mode){
    if (length_current_ms > length_ms){
      //cout << "getLoopEnd() lenght_loop_ms " << lenght_current_ms << " lenght_ms " << lenght_ms << endl;
      setRestart();
      //      start_loop_ms = ofGetElapsedTimeMillis();
    }
  } 
}

ofPixels LoopRecorder::getFrame()
{
   i_play++;
 
   // lenght_play_ms = ofGetElapsedTimeMillis() - start_play_ms;
    // float lll = lenght_play_ms % lenght_ms;
   
    //   if (lenght_play_ms > lenght_ms && abs(lll) < 20){
    // 	cout << "lll " << lll << endl;
    //     i_play = 0;
    // 	//lenght_play_ms = ofGetElapsedTimeMillis() - start_play_ms;
    // 	 cout << "TIMElenght_play_ms " << lenght_play_ms << " lenght_ms " << lenght_ms << endl;
    // 	 //start_play_ms = ofGetElapsedTimeMillis();
    // 	//cout << "if " << loop_lenght -1 << endl;
    //     return frames[loop_lenght-1];
    // }
    if (i_play >= loop_length){
      //cout << "getFrame() i_play " << i_play << " loop_l " << loop_lenght << endl;
      // i_play = 0;
      // lenght_play_ms = ofGetElapsedTimeMillis() - start_play_ms;
      // lenght_loop_ms = 0;
      // start_loop_ms = ofGetElapsedTimeMillis();
      //cout << "a getFrame() lenght_current_ms " << lenght_current_ms << endl;
      setRestart();
      //cout << "b getFrame() lenght_current_ms " << lenght_current_ms << endl;
    	//cout << "FRAMElenght_play_ms " << lenght_play_ms << endl;
      //start_play_ms = ofGetElapsedTimeMillis();
      //cout << "if " << loop_lenght -1 << endl;
      return frames[loop_length-1];
    }else{
      //cout << "else " << i_play - 1<< " ll " << loop_lenght << endl;

  
     return frames[i_play-1 + latence];
      
     }
    //cout << "loop_lenght " << loop_lenght << endl; 
}

ofPixels LoopRecorder::getFrameN(int n)
{
    return frames[n];
}

ofPixels LoopRecorder::getFrameDelay(int delay_ts)
{
  int i_ts = ofGetElapsedTimeMillis();
  int cursor = i_record;
  int looking_ts = i_ts - delay_ts;
  int cpt = 0;
  
  while (i_ts > looking_ts){
    cpt += 1;
    if (cpt >= length_max){
      cursor = i_record;
      break;
    }
    cursor -= 1;
    if (cursor < 0){
      cursor = loop_length-1;
    }
    i_ts = timestamp[cursor];
  }
  return frames[cursor];
}

int LoopRecorder::getCurrentFrameRecord()
{
    return i_record;
}

float LoopRecorder::getCurrentLenghtRecord()
{
  return ofGetElapsedTimeMillis() - start_ms;
}

int LoopRecorder::getCurrentFrameNum()
{
    return i_play;
}

ofPixels LoopRecorder::getCurrentFramePlay()
{
  //cout << " iplay " << i_play << endl;
  return frames[i_play];
}

int LoopRecorder::grabFrameDepth(ofPixels & frame, int depth)
{
    if (record_mode && i_record < length_max){
        frames[i_record] = frame;
        depth_record[i_record] = depth;
        i_record++;
        loop_length = i_record;
        return 1;
    }
    else{
        return 0;
    }
}

int LoopRecorder::getDepthN(int n)
{
    return depth_record[n];
}

LoopRecorder::~LoopRecorder()
{
    //dtor
}
