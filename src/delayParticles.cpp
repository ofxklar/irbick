#include "delayParticles.h"

DelayParticles::DelayParticles(){

}

void DelayParticles::update(){

  std::vector<SimpleParticle> ::iterator iter;
  for(iter = particleList.begin(); iter != particleList.end();) {
      // remove particle if it's NULL
      // if((iter) == NULL) {
      // 	ofLogWarning("ofxParticleManager") << "update(): removing NULL particle";
      // 	iter = particleList.erase(iter);
      // }
    cout << " update vitx " << (iter)->getVit().x << endl;
    cout << " update posx " << (iter)->getPosition().x << endl;
    
    
    
    if ( !(iter)->isAlive() ){
      cout << "is not alive" << endl;
      iter = particleList.erase(iter);
      //iter = particleList.erase(iter);
    }else{
      (iter)->update();
      ++ iter;
    }
    // if((*iter) == NULL) {
    //   ofLogWarning("ofxParticleManager") << "draw(): removing NULL particle";
    //   iter = particleList.erase(iter);
    // }
    
    
  }

  // for (int i = 0; i < particleList.size(); i++){
  //   if (!particleList[i].isAlive()){
  //     particleList.erase(particleList[i]);
  //   }
  // }  

  // cout << " size " << particleList.size() << endl;
  // for (int i = 0; i < particleList.size(); i++){
  //   particleList[i].update();
  // }
}

void DelayParticles::draw(){

  std::vector<SimpleParticle> ::iterator iter;
  for(iter = particleList.begin(); iter != particleList.end();++iter) {
    (iter)->draw();
  }
  // for (int i = 0; i < particleList.size(); i++){
  //   particleList[i].draw();
  // }
}

void DelayParticles::addParticle(ofPoint pos, ofPoint vit, int size, int col, float sat){
  SimpleParticle newParticle(pos, vit, size);
  int r, v, b;

  hsv2rgb(col, r, v, b);
  r = r + (128-r)*(1-sat);
  v = v + (128-v)*(1-sat);
  b = b + (128-b)*(1-sat);
  newParticle.setColor(r, v, b, sat*255);
  particleList.push_back(newParticle);
}

DelayParticles::~DelayParticles(){

}
