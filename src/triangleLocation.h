#ifndef TRIANGLE_LOCATION_H
#define TRIANGLE_LOCATION_H

#include "utils.h"

typedef struct {
  ofPoint pts[3];
  bool visible;
  int index;
  int delay;
  ofPoint center;
  float dist;
}triangleElem;

bool sortDist(triangleElem i,triangleElem j);

class TriangleLocation
{
 public:
  TriangleLocation();
  virtual ~TriangleLocation();
  
  void update();
  void setTriangleWidth(float width);
  void addTriangle();
  void generateKaleidoscope();
  void generateHex(float x, float y);
  void getCenters();
  void getDistFromPoint();
  void sortDistFromPoint();
  void setCenter(ofPoint center_in);
  triangleElem getTriangleElem(int i);
  int getLength();

 protected:
  float tr_width;
  ofPoint center;
  std::vector<triangleElem> triangleList;
};

#endif
