#include "testApp.h"


ofTexture del_tex;
bool i_frame_new;
int i_frame_old;

int cur_angle;
int sample_lenght;
int grab_count;
ofImage grab_screen;
ofFbo fbo;
ofPixels pix_export;
bool b_recording;

// ofxVideoRecorder recorder;

string fileName;
string fileExt;
int sampleRate;
int channels;

vector<ofImage> images;

ofPixels pixels;

int audio_type;
int i_img;

ofPoint cent;

//--------------------------------------------------------------
void testApp::setup(){

  draw_input = true;
  n_delay = 0;

  grab_count = 0;
  ofSetFrameRate(30);

  sender.setup(HOST, 9876);
  receiver.setup(PORT);
  current_msg_string = 0;

  b_recording = false;


  fileName = "export";
  fileExt = ".mov";
  
  // recorder.setVideoCodec("mpeg4"); 
  // recorder.setVideoBitrate("24000k");

  sampleRate = 44100;
  channels = 2;


  pix_export.allocate(1920, 1080, OF_IMAGE_COLOR);
  
  width = 320;
  height = 240;

  fbo.allocate(1920*2, 1080*2, GL_RGB);

  // width = 640;
  // height = 480;

  string parts_dir = "images";
  ofDirectory dir(parts_dir);
  dir.allowExt("jpg");
  int n_imgs = dir.listDir();
  
  for (int i = 0; i < n_imgs; i++){
    string path  = dir.getPath(i);
    ofImage new_img;
    new_img.loadImage(path);
    new_img.resize(width, height);
    images.push_back(new_img);
    ofLogNotice()<<path;
  }

  i_img = 0;

  input_vid.initGrabber(width,height);

  mode_kaleidoscope = true;
  mode_mandala = false;
  mode_grab = false;

  sender_is_setup = true;

  width_m = 240;
  monitor.ratio_x = (float)width_m / width;
  monitor.ratio_y = (float)width_m / width;
  monitor.width = width_m;
  monitor.height = height*monitor.ratio_y;
  monitor.pos_x = 7;
  monitor.pos_y = ofGetHeight() - monitor.height - monitor.pos_x;


  vid_buffer.allocate(width, height, 100, 3);
  vid_buffer.setRecordModeOn();

  delay.init(96,100,0,0,vid_buffer);
  delay.setNsnum(7);
  delay.setDelay(1);
  mode_delay_bis = 0;

  lp_rgb = true;

  if (lp_rgb){
    lp_n_chan = 3;
  }else{
    lp_n_chan = 1;
  }

  lp_buffer.allocate(width, height, 10, lp_n_chan);
  lp_buffer.setRecordModeOn();

  lp_delay.init(96,10,0,0,lp_buffer);
  lp_delay.setNsnum(2);
  lp_delay.setDelay(1);
  lp_n_delay = 10 ;

  lp_mode_black = true;

  im_lp.allocate(width, height, OF_IMAGE_COLOR);

  //im_lp.setColor(ofColor(255));

  input_tex.allocate(width,height,GL_RGBA);
  output_tex.allocate(width,height,GL_RGBA);
  mire.allocate(width,height,OF_IMAGE_COLOR_ALPHA);
  //mire.loadImage("Mire.jpg");

  del_tex.allocate(width, height, GL_RGB);

  rosace_input = "camera";

  center.set(ofGetWidth()/2.0f, ofGetHeight()/2.0f);

  n_drag_points = N_DRAG_POINTS;

  n_triangles = 3;
  mode_free = false;
  angle = 90;
  radius_1 = 150;
  radius_2 = 150;
  radius_3 = 300;
  radius_4 = 300;
  n_angles = 4;

  for (int i = 0; i < n_drag_points; i++){
    drag_points[i].bOver = false;
    drag_points[i].bBeingDragged = false;
    drag_points[i].radius = 5;
  }

  cent.set(width/2, height/2);  
  tex_coord[0].set(0,height);
  tex_coord[1].set(width/2.0,0);
  tex_coord[2].set(width,height);

  drag_points[0].pt = tex_coord[0];
  drag_points[1].pt = tex_coord[1];
  drag_points[2].pt = tex_coord[2];

  radius_in = height/2;
  angle_in = -90;
  setTexCoord(radius_in, angle_in);

  triangle_coord[0][0] = drag_points[3].pt -50;
  triangle_coord[0][1] = drag_points[4].pt -50;
  triangle_coord[0][2] = drag_points[5].pt -50;

  drag_points[3].pt.set(50,50);
  drag_points[4].pt.set(200,50);
  drag_points[5].pt.set(200,100);

  drag_points[6].pt.set(200, 50);
  drag_points[7].pt.set(200,100);
  drag_points[8].pt.set(250,150);

  drag_points[9].pt.set(200,50);
  drag_points[10].pt.set(360,50);
  drag_points[11].pt.set(250,150);

  tr_location.setCenter(center);
  tr_location.setTriangleWidth(200);
  tr_location.generateKaleidoscope();

  n_triangles_k = 11;

  grab_radius_1 = 500;
  grab_radius_2 = 300;

  grab_angle = 3;

  time_to_turn = 10000;

  draw_audio = true;
  draw_grab = true;
  mode_audio = false;

  audio_type = 0;
  int bufferSize		= 512;
  sampleRate 			= 44100;
  volume			= 0.5f;

  sample_lenght = 1000;
  step_by_tour = 360.0/grab_angle/sample_lenght;
  lAudio.assign(bufferSize, 0.0);
  rAudio.assign(bufferSize, 0.0);

  cur_angle = 0;

  soundStream.setup(this, 2, 0, sampleRate, bufferSize, 4);	
  
  setupGUI();
}

void testApp::setupGUI(){
  
  gui.setup("IRBIK");
  gui.add(draw_gui.set("draw gui", true));
  gui.add(draw_input.set("draw input", true));
  gui.add(n_delay.set("delay", 0.0, 0.0, 4.0));
  gui.add(radius_1.set("radius_1", 150, 1, 4000));
  gui.add(angle_in.set("angle_in", 0.0, 0.0, 360));
  gui.add(radius_in.set("radius_in", 180.0, 0.0, 360));

  angle_in.addListener(this, &testApp::coordChanged);
  radius_in.addListener(this, &testApp::coordChanged);

  radius_1.addListener(this, &testApp::radiusChanged);
}

void testApp::coordChanged(float & x){
  setTexCoord(radius_in, angle_in);
}

void testApp::radiusChanged(float & x){
  // tr_location.setTriangleWidth(radius_1);
    if (mode_kaleidoscope){tr_location.setTriangleWidth(radius_1);}
}

// void testApp::setupGUI(){
//   float dim = 16;
//   float xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
//   float length = 255-xInit;
  
//   gui = new ofxUICanvas(0, 0, length+xInit, ofGetHeight());
//   gui->setTheme(OFX_UI_THEME_MINGREEN);
//   gui->addWidgetDown(new ofxUILabel("IRBIK Panel 1", OFX_UI_FONT_MEDIUM));
//   gui->addWidgetDown(new ofxUILabel("Press 'g' to Hide GUI", OFX_UI_FONT_MEDIUM));
//   gui->addWidgetDown(new ofxUILabel("Press 'i' to Hide Input Texture", OFX_UI_FONT_MEDIUM));
//   gui->addSpacer(length-xInit, 2);

//   //  gui->addWidgetDown(new ofxUILabel("source :", OFX_UI_FONT_MEDIUM));


//   vector<string> names;
//   names.push_back("kaleidoscope");
//   names.push_back("mandala");
//   names.push_back("full");

//   gui->addRadio("VISU", names, OFX_UI_ORIENTATION_HORIZONTAL, dim, dim);

//   vector<string> names1;
//   names1.push_back("camera");
//   names1.push_back("mire");
//   names1.push_back("screen");
//   names1.push_back("delay");

//   gui->addRadio("SOURCE", names1, OFX_UI_ORIENTATION_HORIZONTAL, dim, dim);
    
//   gui->addSlider("N_ANGLES", 2.0, 50.0, 4.0, length-xInit, dim);
    
//   gui->addSlider("RADIUS_1", 20.0, 700.0, 150.0, length-xInit, dim);
//   gui->addSlider("RADIUS_2", 0.0, 700.0, 150.0, length-xInit, dim);
//   gui->addSlider("RADIUS_3", 0.0, 700.0, 300.0, length-xInit, dim);
//   gui->addSlider("RADIUS_4", 0.0, 700.0, 300.0, length-xInit, dim);
//   gui->addSlider("DELAY", 0.0, 4.0, 0.0, length-xInit, dim);

//   vector<string> names2;
//   names2.push_back("triangle");
//   names2.push_back("etoile");
//   gui->addRadio("mode delay", names2, OFX_UI_ORIENTATION_HORIZONTAL, dim, dim);
 
//   gui->addSlider("N_TRIANGLES", 0, 11, 10, length-xInit, dim);

//   gui->addSpacer(length-xInit, 2);

//   // gui->addSlider("N_LP", 0.0, 100.0, 0.0, length-xInit, dim);
//   // gui->addSlider("DEC_LP", 0.0, 10.0, 0.0, length-xInit, dim);
  
//   // gui->addSlider("MAP_B", -254, 254.0, 0.0, length-xInit, dim);
//   // gui->addSlider("MAP_W", 0, 500, 255, length-xInit, dim);

//   // vector<string> names3;
//   // names3.push_back("white");
//   // names3.push_back("black");
//   // gui->addRadio("mode light painting", names3, OFX_UI_ORIENTATION_HORIZONTAL, dim, dim);

//   gui->addSlider("GRAB_ANGLE", 1.0, 12.0, 3.0, length-xInit, dim);
//   gui->addSlider("SAMPLE_LENGHT", 10.0, 1024.0, 440.0, length-xInit, dim);
//   gui->addSlider("GRAB_1", 0.0, 700.0, 500.0, length-xInit, dim);
//   gui->addSlider("GRAB_2", 0.0, 700.0, 300.0, length-xInit, dim);

//   gui->addSlider("REVOLUTION", 0.0, 10.0, 4.0, length-xInit, dim);

//   gui->addSlider("RADIUS_IN", 0, height, height, length-xInit, dim);
//   gui->addSlider("ANGLE_IN", 0.0, 360.0, 0.0, length-xInit, dim);
      
//   gui->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_MEDIUM)); 


//   ofAddListener(gui->newGUIEvent,this,&testApp::guiEvent);
//   gui->toggleVisible();
// }

void testApp::updateLp(){
  ofPixels pix;
  ofPixels pix_base;

  pix = input_vid.getPixelsRef();

  if (lp_rgb){
    pix_base.allocate(width, height, OF_IMAGE_COLOR);
  }else{
    pix_base.allocate(width, height, OF_IMAGE_GRAYSCALE);
    pix.setImageType(OF_IMAGE_GRAYSCALE);
  }
  
  for (int i = 0; i<height*width*lp_n_chan;i++){
    pix[i] = max(ofMap(float(pix[i]), 0.0, 255.0, map_b, map_w),float(0));
  }

  if (lp_mode_black == false){
    pix_base.setColor(254);
  }else{
    pix_base.setColor(0);
  }
  
  lp_buffer.grabFrame(pix);

  for (int i = 1; i<lp_n_delay; i++){
    ofPixels pix_buf;
    int i_frame = lp_delay.getFramePos(i, lp_buffer);
    pix_buf = lp_buffer.getFrameN(i_frame);

    if (lp_mode_black){
      maxImagePix(pix_base, pix_buf, lp_n_chan);
    }else{
      minImagePix(pix_base, pix_buf, lp_n_chan);
    }  
  }

  if (lp_rgb == false){
    pix_base.setImageType(OF_IMAGE_COLOR);
  }
  im_lp.setFromPixels(pix_base);
}


//--------------------------------------------------------------
void testApp::update(){
  elapsedTime		= ofGetElapsedTimef();
  appFrameCount	= ofGetFrameNum();
  appFrameRate	= ofGetFrameRate();
  
  //ofLogNotice() << ofGetFrameRate();

  //delay_particles.update();

  ofBackground(0,0,0);
  
  input_vid.update();


  if(input_vid.isFrameNew()){

 
    int i_rec;
    if (rosace_input == "camera"){
      pixels = input_vid.getPixelsRef();
      i_rec = vid_buffer.grabFrame(pixels);
      vid_buffer.setData(i_rec, new FrameData());
      FrameData * fd = (FrameData*)vid_buffer.getData(i_rec);
      fd->pt[0] = drag_points[0].pt;
      fd->pt[1] = drag_points[1].pt;
      fd->pt[2] = drag_points[2].pt;
      
    }else if (rosace_input == "mire"){
      i_rec = vid_buffer.grabFrame(pixels);
      vid_buffer.setData(i_rec, new FrameData());
      FrameData * fd = (FrameData*)vid_buffer.getData(i_rec);
      fd->pt[0] = drag_points[0].pt;
      fd->pt[1] = drag_points[1].pt;
      fd->pt[2] = drag_points[2].pt;
    

    //pixels = mire.getPixelsRef();
    }else if (rosace_input == "screen"){
      pixels = im_screen.getPixelsRef();
      vid_buffer.grabFrame(pixels);
    }else if (rosace_input == "lp"){
      updateLp();
      pixels = im_lp.getPixelsRef();
      vid_buffer.grabFrame(pixels);
      lp_delay.update();
    }
    delay.update();
    output_tex.loadData(pixels);
  }

  tex_coord[0] = drag_points[0].pt;
  tex_coord[1] = drag_points[1].pt;
  tex_coord[2] = drag_points[2].pt;

  updateOsc();

  if (mode_free){
    triangle_coord[0][0] = drag_points[3].pt -50;
    triangle_coord[0][1] = drag_points[4].pt -50;
    triangle_coord[0][2] = drag_points[5].pt -50;

    triangle_coord[1][0] = drag_points[6].pt -50;
    triangle_coord[1][1] = drag_points[7].pt -50;
    triangle_coord[1][2] = drag_points[8].pt -50;

    triangle_coord[2][0] = drag_points[9].pt -50;
    triangle_coord[2][1] = drag_points[10].pt -50;
    triangle_coord[2][2] = drag_points[11].pt -50;

  }else{
    angle = 180.0/n_angles;

    ofPoint pt1, pt2, pt3, pt4;
    pt1.set(radius_1, 0);
    pt2.set(radius_2*cos(ofDegToRad(angle)),radius_2*sin(ofDegToRad(angle)));
    pt3.set(radius_3,0);
    pt4.set(radius_4*cos(ofDegToRad(angle)),radius_4*sin(ofDegToRad(angle)));

    triangle_coord[0][0].set(0,0);
    triangle_coord[0][1] = pt1;
    triangle_coord[0][2] = pt2;

    triangle_coord[1][0] = pt1;
    triangle_coord[1][1] = pt2;
    triangle_coord[1][2] = pt4;

    triangle_coord[2][0] = pt3;
    triangle_coord[2][1] = pt1;
    triangle_coord[2][2] = pt4;
  }
}

void testApp::setTexCoord(int radius, int angle){

  
  for (int i = 0; i<3; i++){
    drag_points[i].pt.set(cent.x + (radius * cos(ofDegToRad(angle + i * 120))), 
			  cent.y + (radius * sin(ofDegToRad(angle + i * 120))));  
  }
}

void testApp::updateOsc(){
  while(receiver.hasWaitingMessages()){
    // get the next message
    ofxOscMessage m;
    receiver.getNextMessage(&m);
    cout << "receice osc\n"<<endl;
    cout << m.getAddress( ) << endl;
    // check for mouse moved message
    if(m.getAddress() == "/irbick/n_angles"){
      n_angles = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/angle_in"){
      angle_in = m.getArgAsInt32(0);
      setTexCoord(radius_in, angle_in);
    }else if(m.getAddress().substr(0,m.getAddress().size() - 1) == "/irbick/points/"){
      int num_point = ofToInt(m.getAddress().substr(m.getAddress().size() - 1));
      drag_points[num_point-1].pt.set(m.getArgAsFloat(1) * width, height - m.getArgAsFloat(0) * height); 
      cout << "x " << m.getArgAsFloat(0) << " y " << m.getArgAsFloat(1) << endl;
    }else if(m.getAddress() == "/irbick/triangle_center"){
      cent.set(m.getArgAsFloat(1)*width, (1-m.getArgAsFloat(0))*height);
      setTexCoord(radius_in, angle_in);
    }else if(m.getAddress() == "/irbick/delay"){
      n_delay = m.getArgAsInt32(0);
      delay.setDelay(n_delay);
    }else if(m.getAddress() == "/irbick/radius_in"){
      radius_in = m.getArgAsInt32(0);
      setTexCoord(radius_in, angle_in);
    }else if(m.getAddress() == "/irbick/angle_rotation"){
      angle_rotation = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/radius_1"){
      radius_1 = m.getArgAsInt32(0);
      if (mode_kaleidoscope){tr_location.setTriangleWidth(radius_1);}
    }else if(m.getAddress() == "/irbick/radius_2"){
      radius_2 = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/radius_3"){
      radius_3 = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/radius_4"){
      radius_4 = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/n_triangles"){
      n_triangles_k = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/grab_radius_1"){
      grab_radius_1 = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/grab_radius_2"){
      grab_radius_2 = m.getArgAsInt32(0);
    }else if(m.getAddress() == "/irbick/grab_angle"){
      grab_angle = m.getArgAsInt32(0);
      step_by_tour = 360.0/grab_angle/sample_lenght;
    }else if(m.getAddress() == "/irbick/volume"){
      volume = m.getArgAsFloat(0);

    }else if(m.getAddress().substr(0,16) == "/irbick/keyboard"){
      if (m.getArgAsInt32(0) !=0){
	int key_y = ofToInt(m.getAddress().substr(17,18));
	int key_x = ofToInt(m.getAddress().substr(19));
	
	cout << "toto " << key_x<< endl;
	int key_note = 12 * key_y + key_x;
	float key_freq = 440* pow(2,(key_note-69.0)/12);
	sample_lenght = 44100/key_freq;
	step_by_tour = 360.0/grab_angle/sample_lenght;
      }
    }else if (m.getAddress() == "/irbick/freq"){
      sample_lenght = 44100/m.getArgAsInt32(0);
      step_by_tour = 360.0/grab_angle/sample_lenght;
    }else if(m.getAddress() == "/irbick/sample_lenght"){
      sample_lenght = m.getArgAsInt32(0);
      step_by_tour = (360.0f/grab_angle)/float(sample_lenght);
    }else if(m.getAddress() == "/irbick/record"){
      if (m.getArgAsInt32(0) != 1){
	b_recording = false;
      }else{
	b_recording = true;
	// if(b_recording && !recorder.isInitialized()) {
	//   recorder.setup(fileName+ofGetTimestampString()+fileExt, 
	// 		 1920, 
	// 		 1080, 
	// 		 24,
	// 		 sampleRate,
	// 		 channels);
	// }
      }
    }else if(m.getAddress() == "/irbick/save"){
      b_recording = false;
      // recorder.close();
     
    }else if((m.getAddress() == "/irbick/prev") and (m.getArgAsInt32(0)!=0)){
     
      i_img --;
      if (i_img < 0){
	i_img = images.size()-1;
      }
      pixels = images[i_img].getPixelsRef();
     
    }else if((m.getAddress() == "/irbick/next") and (m.getArgAsInt32(0)!=0)){
      i_img ++;
      if (i_img >= images.size()){
	i_img = 0;
      }
      pixels = images[i_img].getPixelsRef();
     
    }else if(m.getAddress() == "/irbick/note"){
      int num_note = m.getArgAsInt32(0);
      int val_note = m.getArgAsInt32(1);

      ofPoint vitesse;
      ofPoint pos;
      int col = ofMap(num_note, 48, 72, 0, 255);
      float sat = 1;
      vitesse.set(0, -300);
      pos.set(ofMap(num_note%12, 0, 12, 0, width), height);
    
      delay_particles.addParticle(pos, 
				  vitesse, 
				  val_note/2, 
				  col, 
				  sat);

      cout << "num "<< num_note << " val " << val_note << endl;
    }		else{
      // unrecognized message: display on the bottom of the screen
      string msg_string;
      msg_string = m.getAddress();
      msg_string += ": ";
      for(int i = 0; i < m.getNumArgs(); i++){
	// get the argument type
	msg_string += m.getArgTypeName(i);
	msg_string += ":";
	// display the argument - make sure we get the right type
	if(m.getArgType(i) == OFXOSC_TYPE_INT32){
	  msg_string += ofToString(m.getArgAsInt32(i));
	}
	else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
	  msg_string += ofToString(m.getArgAsFloat(i));
	}
	else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
	  msg_string += m.getArgAsString(i);
	}
	else{
	  msg_string += "unknown";
	}
      }
      // add to the list of strings to display
      msg_strings[current_msg_string] = msg_string;
      timers[current_msg_string] = ofGetElapsedTimef() + 5.0f;
      current_msg_string = (current_msg_string + 1) % NUM_MSG_STRINGS;
      // clear the next line
      msg_strings[current_msg_string] = "";
    }
  }

  if (ofGetFrameNum() == 60){
    //sender.setup(HOST, OUTPORT);
    sender_is_setup = true;
    receiver.setup(PORT);
  }
}


//--------------------------------------------------------------
void testApp::draw(){
  fbo.begin();
  if (rosace_input == "screen"){
    drawNote();
    im_screen.grabScreen(monitor.pos_x, monitor.pos_y, monitor.width, monitor.height);
    im_screen.resize(width, height);
  }

  if (mode_mandala){
    drawMandala();
  }else if (mode_kaleidoscope){
    drawKaleidoscopeBis();
  }else if (mode_full){
    int h = ofGetHeight();
    im_lp.draw(center.x - h*2/3.0, 0, h*4/3.0, h);  
  }


  fbo.end();
  fbo.draw(0,0);

  if (mode_grab ){
    grabNote();
  }    

  if (draw_input){
    drawInput(monitor);
  }

  if (b_recording){
    fbo.readToPixels(pix_export);
    // recorder.addFrame(pix_export);
  }
  if (draw_gui){
    gui.draw();
  }
}

void testApp::grabNote(){
  ofPoint grab_pos;
  int t = ofGetElapsedTimeMillis() % time_to_turn; 
  float a = ofMap(t, 0, time_to_turn, 0, 360);

  grab_pos.set(center.x + cos(a*PI/180)*grab_radius_1, center.y + sin(a*PI/180)*grab_radius_1);

  grab.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
  ofColor grab_color = grab.getColor(grab_pos.x, grab_pos.y);
  if (sender_is_setup){
    ofxOscMessage m;
    m.setAddress("/texture");
    m.addIntArg(grab_color.r);
    m.addIntArg(grab_color.g);
    m.addIntArg(grab_color.b);
      sender.sendMessage(m);
  }

  // ofPushStyle();
  // ofSetColor(grab_color);
  // ofCircle(grab_pos, 40);
  // ofPopStyle();
  
  if (draw_audio){
    drawAudio();
  }

  if (draw_grab){
    drawGrab();
  }

}

void testApp::drawAudio(){
  // draw the left channel:
  int posx = monitor.pos_x * 2 + monitor.width;
  int posy = monitor.pos_y;
  int sizex = ofGetWidth() - posx  - monitor.pos_x;
  int sizey = monitor.height;

  ofPushStyle();
  ofPushMatrix();

  ofTranslate(posx, posy, 0);
  
  ofSetColor(225);
  ofDrawBitmapString("Left Channel", 4, 18);
  ofFill();
  ofSetLineWidth(1);	
  ofSetColor(255, 255, 255, 150);
  ofRect(0, 0, sizex, sizey);

  ofNoFill();  
  ofSetColor(245, 58, 135);
  ofSetLineWidth(2);
					
  ofBeginShape();
  for (unsigned int i = 0; i < lAudio.size(); i++){
    float x =  ofMap(i, 0, lAudio.size(), 0, sizex, true);
    //ofVertex(x, sizey - (lAudio[i] / volume) *sizey);
    ofVertex(x,  sizey * lAudio[i] + (sizey/2));
  }
  ofEndShape(false);
  ofSetColor(58, 135, 245);
  ofBeginShape();
  for (unsigned int i = 0; i < rAudio.size(); i++){
    float x =  ofMap(i, 0, rAudio.size(), 0, sizex, true);
    //ofVertex(x, sizey - (rAudio[i] / volume) *sizey);
    ofVertex(x, rAudio[i] * sizey + (sizey/2));
  }
  ofEndShape(false);

  ofPopMatrix();
  ofPopStyle();
}

void testApp::drawGrab(){
  ofPushStyle();
  ofNoFill();
  ofSetLineWidth(1);
  ofSetColor(255);
  ofCircle(center, grab_radius_1);
  ofCircle(center, grab_radius_2);

  ofFill();
  //ofCircle(volume_pos, ofMap(volume, 0, 1, 0, 50));
  ofPopStyle();
}

void testApp::drawNote(){
  ofPushMatrix();
  ofTranslate(monitor.pos_x, monitor.pos_y);
  ofScale(monitor.ratio_x, monitor.ratio_y);
  ofPushStyle();
  // for (int i = 0; i<100;i++){
  //   if (liste_note[i] != 0){
  //     int r, v, b;
  //     int col = ofMap(i, 48, 72, 0, 255);
  //     hsv2rgb(col, r, v, b);
  //     ofSetColor(r, v, b);
  //     cout <<" r "<<r<< " v "<<v<< " b "<<b<<endl;
  //     ofCircle(width/2, height/2, liste_note[i]);
  //   }
  // }
  delay_particles.draw();
  ofPopStyle();
  ofPopMatrix();
}

void testApp::drawKaleidoscopeBis(){
  cpt_delay = 0;
  ofPushStyle();
  glEnable(GL_TEXTURE_2D);

  if (!n_delay){
    output_tex.bind();
  }
  
  // nombre de triangles d un kaleidoscope
  int n_kalei = 6 * n_triangles_k * n_triangles_k;

    //int n_triangle = min(n_triangles_tot[n_triangles_k], tr_location.getLength());
    int n_triangle = min(n_kalei, tr_location.getLength());

  for (int i = 0; i < n_triangle; i++){
    triangleElem tr = tr_location.getTriangleElem(i);
    if (tr.visible){
      drawTriangleDel2(tex_coord, tr.pts, tr.delay);
    }
  }
  if (!n_delay){
    output_tex.unbind();
  }

  glDisable(GL_TEXTURE_2D);
  ofPopStyle();
  
}

void testApp::drawKaleidoscope(){
  cpt_delay = 0;
  int radius = radius_1;
  ofPushStyle();
  glEnable(GL_TEXTURE_2D);

  if (!n_delay){
    output_tex.bind();
  }


  ofPushMatrix();
  ofTranslate(center.x,center.y);

  for (int i = 0; i < 6; i++){
    ofPoint tr[3];
    int angle = i * 60;

    tr[0].set(0,0);        
    if (i%2){
      tr[1].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      angle += 60;
      tr[2].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
    }else{
      tr[2].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      angle += 60;
      tr[1].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
    }

    drawTriangleDel(tex_coord, tr);
  }
  for (int i = 0; i < 6; i++){
    ofPoint tr[3];
    int angle = i * 60;

    if (i%2){
      tr[1].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      tr[2].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      angle +=30;
      tr[0].set(cos(angle*PI/180)*sqrt(3)*radius, sin(angle*PI/180)*sqrt(3)*radius);
      drawTriangleDel(tex_coord, tr);

      angle +=30;
      tr[2].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      drawTriangleDel(tex_coord, tr);

      tr[1].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      drawTriangleDel(tex_coord, tr);
    }else{
      tr[2].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      tr[1].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      angle +=30;
      tr[0].set(cos(angle*PI/180)*sqrt(3)*radius, sin(angle*PI/180)*sqrt(3)*radius);
      drawTriangleDel(tex_coord, tr);
      
      angle +=30;
      tr[1].set(cos(angle*PI/180)*radius, sin(angle*PI/180)*radius);
      drawTriangleDel(tex_coord, tr);

      tr[2].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      drawTriangleDel(tex_coord, tr);
    }
  }

  for (int i = 0; i < 6; i++){
    ofPoint tr[3];
    int angle = i * 60;

    if (i%2){
      tr[2].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      tr[0].set(cos(angle*PI/180)*3*radius, sin(angle*PI/180)*3*radius);
      angle +=20;
      tr[1].set(cos(angle*PI/180)*2.5*radius/cos(20*PI/180), sin(angle*PI/180)*2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 10;
      tr[0].set(cos(angle*PI/180)*sqrt(3)*radius, sin(angle*PI/180)*sqrt(3)*radius);
      drawTriangleDel(tex_coord, tr);

      angle += 10;
      tr[2].set(cos(angle*PI/180)*2.5*radius/cos(20*PI/180), sin(angle*PI/180)*2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 20;
      tr[1].set(cos(angle*PI/180)*2*radius, sin(angle*PI/180)*2*radius);
      drawTriangleDel(tex_coord, tr);

      tr[0].set(cos(angle*PI/180)*3*radius, sin(angle*PI/180)*3*radius);
      drawTriangleDel(tex_coord, tr);

    }else{
      tr[1].set(cos(angle*PI/180) *2*radius, 
		sin(angle*PI/180) *2*radius);
      tr[0].set(cos(angle*PI/180) *3*radius, 
		sin(angle*PI/180) *3*radius);
      angle +=20;
      tr[2].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 10;
      tr[0].set(cos(angle*PI/180) *sqrt(3)*radius, 
		sin(angle*PI/180) *sqrt(3)*radius);
      drawTriangleDel(tex_coord, tr);
      
      angle += 10;
      tr[1].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);
      
      angle += 20;
      tr[2].set(cos(angle*PI/180) * 2*radius, 
		sin(angle*PI/180) * 2*radius);
      drawTriangleDel(tex_coord, tr);

      tr[0].set(cos(angle*PI/180) * 3*radius, 
		sin(angle*PI/180) * 3*radius);
      drawTriangleDel(tex_coord, tr);
    }
  }

  for (int i = 0; i < 6; i++){
    ofPoint tr[3];
    int angle = i * 60;

    if (i%2){
      tr[0].set(cos(angle*PI/180) *3*radius, 
       		sin(angle*PI/180) *3*radius);
      tr[1].set(cos(angle*PI/180) *4*radius, 
      		sin(angle*PI/180) *4*radius);
      angle +=15;
      tr[2].set(cos(angle*PI/180) *3.5*radius/cos(15*PI/180), 
      		sin(angle*PI/180) *3.5*radius/cos(15*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 5;
      tr[1].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
       		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 10;
      tr[0].set(cos(angle*PI/180) *2*sqrt(3)*radius, 
       		sin(angle*PI/180) *2*sqrt(3)*radius);
       drawTriangleDel(tex_coord, tr);
 
      angle += 10;
      tr[2].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
      		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 5;
      tr[1].set(cos(angle*PI/180) *3.5*radius/cos(15*PI/180), 
      		sin(angle*PI/180) *3.5*radius/cos(15*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 15;
      tr[0].set(cos(angle*PI/180) *3*radius, 
      		sin(angle*PI/180) *3*radius);
      drawTriangleDel(tex_coord, tr);

      tr[2].set(cos(angle*PI/180) *4*radius, 
      		sin(angle*PI/180) *4*radius);
      drawTriangleDel(tex_coord, tr); 

    }else{
      tr[0].set(cos(angle*PI/180) *3*radius, 
		sin(angle*PI/180) *3*radius);
      tr[2].set(cos(angle*PI/180) *4*radius, 
		sin(angle*PI/180) *4*radius);
      angle +=15;
      tr[1].set(cos(angle*PI/180) *3.5*radius/cos(15*PI/180), 
		sin(angle*PI/180) *3.5*radius/cos(15*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 5;
      tr[2].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 10;
      tr[0].set(cos(angle*PI/180) *2*sqrt(3)*radius, 
		sin(angle*PI/180) *2*sqrt(3)*radius);
      drawTriangleDel(tex_coord, tr);
 
      angle += 10;
      tr[1].set(cos(angle*PI/180) *2.5*radius/cos(20*PI/180), 
		sin(angle*PI/180) *2.5*radius/cos(20*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 5;
      tr[2].set(cos(angle*PI/180) *3.5*radius/cos(15*PI/180), 
		sin(angle*PI/180) *3.5*radius/cos(15*PI/180));
      drawTriangleDel(tex_coord, tr);

      angle += 15;
      tr[0].set(cos(angle*PI/180) *3*radius, 
		sin(angle*PI/180) *3*radius);
      drawTriangleDel(tex_coord, tr);

      tr[1].set(cos(angle*PI/180) *4*radius, 
		sin(angle*PI/180) *4*radius);
      drawTriangleDel(tex_coord, tr); 
    }
  }  

  ofPopMatrix();
  
  if (!n_delay){
    output_tex.unbind();
  }

  glDisable(GL_TEXTURE_2D);
  ofPopStyle();
  
}

void testApp::drawMandala(){
  cpt_delay = 0;
  ofPushStyle();
  glEnable(GL_TEXTURE_2D);

  if (!n_delay){
    output_tex.bind();
  }
  ofPushMatrix();
  ofTranslate(center.x,center.y);
  ofRotate(-90 + angle_rotation);

  for (int j = 0; j<n_angles;j++){
    if ((rosace_input == "camera" or rosace_input == "screen" or rosace_input == "lp") and  n_delay>0){
      ofTexture out_tex;
      int i_frame = delay.getFramePos(j, vid_buffer);

      out_tex.allocate(width, height, GL_RGB);
      out_tex.loadData(vid_buffer.getFrameN(i_frame));
      out_tex.bind();
      ofPushMatrix();
        ofRotate(j*angle*2);
        drawAngle();
      ofPopMatrix();
      out_tex.unbind();
    }else if (mode_delay_bis){
      ofPushMatrix();
        ofRotate(j*angle*2);
        drawAngleDelay();
      ofPopMatrix();
    }else{
      ofPushMatrix();
        ofRotate(j*angle*2);
        drawAngle();
      ofPopMatrix();
    }
  }
  
  ofPopMatrix();
  
  if (!n_delay){
    output_tex.unbind();
  }

  glDisable(GL_TEXTURE_2D);
  ofPopStyle();
}

void testApp::drawInput(monitorRef monitor){
  ofPushMatrix();
  ofTranslate(monitor.pos_x, monitor.pos_y);
  ofScale(monitor.ratio_x, monitor.ratio_y);
  ofPushStyle();
    output_tex.draw(0,0, width, height);
  ofPopStyle();

  ofPushStyle();
    ofNoFill();
    ofSetColor(200, 200, 200, 200);
    for (int i = 0; i<1;i++){
      ofBeginShape();
      for (int j = i*3; j<(i+1)*3; j++){
	ofVertex(drag_points[j].pt.x, drag_points[j].pt.y);
	if (drag_points[j].bOver == true) ofFill();
	else ofNoFill();
	ofCircle(drag_points[j].pt.x, drag_points[j].pt.y,drag_points[j].radius / monitor.ratio_x);
	ofNoFill();
      }
      ofEndShape(true);
    }
    ofSetColor(255,255,255);
  ofPopStyle();
  ofPopMatrix();
}

void testApp::drawAngle(){
  for (int i = 0; i<n_triangles;i++)
    {
      ofPushMatrix();
      drawTriangle(tex_coord, triangle_coord[i]);
      ofScale(1,-1);
      drawTriangle(tex_coord, triangle_coord[i]);
      ofPopMatrix();
    }
}

void testApp::drawTriangle(ofPoint tex_coord[3], ofPoint triangle_coord[3]){
  glBegin(GL_TRIANGLES);
  for (int i=0;i<3;i++){
    glTexCoord2f(tex_coord[i].x,tex_coord[i].y);
    glVertex3i(triangle_coord[i].x,triangle_coord[i].y,0);
  }
  glEnd();
}

void testApp::drawTriangleDel(ofPoint tex_coord[3], ofPoint triangle_coord[3]){
  ofTexture out_tex;
  if (n_delay){
    out_tex.allocate(width, height, GL_RGB);
    int i_frame = delay.getFramePos((int)round(cpt_delay), vid_buffer);
    out_tex.loadData(vid_buffer.getFrameN(i_frame));
    out_tex.bind();
  }
  glBegin(GL_TRIANGLES);
  for (int i=0;i<3;i++){
    glTexCoord2f(tex_coord[i].x,tex_coord[i].y);
    glVertex3i(triangle_coord[i].x,triangle_coord[i].y,0);
  }
  glEnd();
  if (n_delay){
    out_tex.unbind();
    cpt_delay ++;
  }
}

void testApp::drawTriangleDel2(ofPoint tex_coord[3], ofPoint triangle_coord[3], int del){
  //ofTexture del_tex;
  ofPoint pt_cur[3];
  pt_cur[0] = tex_coord[0];
  pt_cur[1] = tex_coord[1];
  pt_cur[2] = tex_coord[2];
  FrameData * fd;

  if (n_delay){
    //del_tex.allocate(width, height, GL_RGB);
    int i_frame = delay.getFramePos(del, vid_buffer);
    if (i_frame != i_frame_old){
      del_tex.unbind();
      del_tex.loadData(vid_buffer.getFrameN(i_frame));
      i_frame_old = i_frame;
      del_tex.bind();


    }
    //del_tex.bind();

    fd = (FrameData*)vid_buffer.getData(i_frame);
    pt_cur[0] = fd->pt[0];
    pt_cur[1] = fd->pt[1];
    pt_cur[2] = fd->pt[2];

  }
  glBegin(GL_TRIANGLES);
  for (int i=0;i<3;i++){
    glTexCoord2d(pt_cur[i].x,pt_cur[i].y);
    glVertex3i(triangle_coord[i].x,triangle_coord[i].y,0);
  }
  glEnd();

  if (n_delay){
    //del_tex.unbind();
  }
}

void testApp::drawAngleDelay(){
  for (int i = 0; i<n_triangles;i++){
    ofTexture out_tex;
    out_tex.allocate(width, height, GL_RGB);
    int i_frame = delay.getFramePos((int)round(cpt_delay), vid_buffer);
    out_tex.loadData(vid_buffer.getFrameN(i_frame));
    out_tex.bind();
    ofPushMatrix();
      drawTriangleDelay(tex_coord, triangle_coord[i]);
      ofScale(1,-1);
      drawTriangleDelay(tex_coord, triangle_coord[i]);
    ofPopMatrix();
    out_tex.unbind();
    cpt_delay ++;
  }
}

void testApp::drawTriangleDelay(ofPoint tex_coord[3], ofPoint triangle_coord[3]){
  glBegin(GL_TRIANGLES);
  for (int i=0;i<3;i++){
    glTexCoord2f(tex_coord[i].x,tex_coord[i].y);
    glVertex3i(triangle_coord[i].x,triangle_coord[i].y,0);
  }
  glEnd();

}
//--------------------------------------------------------------
void testApp::keyPressed(int key){
  ofLogNotice() << key;
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    draw_gui = !draw_gui;
    break;
  case 'i':
    draw_input = !draw_input;
    break;
  case 't':
    input_vid.videoSettings();
    break;
  case 'a':
    mode_audio = !mode_audio;
    mode_grab = !mode_grab;
    break;
  case 'm':
    if (audio_type == 0){
      audio_type = 1;
    }else{
      audio_type = 0;
    }
    break;
  case 'z':
    draw_audio = !draw_audio;
    break;
    //case 's':
    //ofxOscMessage m;
    //m.setAddress("/texture");
    //m.addIntArg(grab_color.r);
    //m.addIntArg(grab_color.g);
    //m.addIntArg(grab_color.b);
    //sender.sendMessage(m);
    //break;
  // case 'r':
  //   b_recording = !b_recording;
  //   if(b_recording && !recorder.isInitialized()) {
  //           recorder.setup(fileName+ofGetTimestampString()+fileExt, 
	// 		   1920, 
	// 		   1080, 
	// 		   24,
	// 		   sampleRate,
	// 		   channels);
  //   }
  //   break;
  // case 's':
  //   b_recording = false;
  //   recorder.close();
  //   break;
  case 'd':
    draw_grab = !draw_grab;
    break;
  case 356:
    i_img --;
    if (i_img < 0){
      i_img = images.size()-1;
    }
    pixels = images[i_img].getPixelsRef();
    break;
  case 358:
    i_img ++;
    if (i_img >= images.size()){
      i_img = 0;
    }
    pixels = images[i_img].getPixelsRef();
    break;
  }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}


//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
  ofPoint xy = changeRef(ofPoint(x, y), monitor);
  for (int i = 0; i < n_drag_points; i++){
    float diffx = xy.x - drag_points[i].pt.x;
    float diffy = xy.y - drag_points[i].pt.y;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < drag_points[i].radius / monitor.ratio_x){
      drag_points[i].bOver = true;
    } else {
      drag_points[i].bOver = false;
    }	
  }

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
  ofPoint xy = changeRef(ofPoint(x, y), monitor);
  for (int i = 0; i < n_drag_points; i++){
    if (drag_points[i].bBeingDragged == true){
      drag_points[i].pt.x = xy.x;
      drag_points[i].pt.y = xy.y;
    }
  }
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
  ofPoint xy = changeRef(ofPoint(x, y), monitor);
  for (int i = 0; i < n_drag_points; i++){
    float diffx = xy.x - drag_points[i].pt.x;
    float diffy = xy.y - drag_points[i].pt.y;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < drag_points[i].radius / monitor.ratio_x){
      drag_points[i].bBeingDragged = true;
    } else {
      drag_points[i].bBeingDragged = false;
    }	
  }
  //tr_location.setCenter(ofPoint(x,y));
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
  for (int i = 0; i < n_drag_points; i++){
    drag_points[i].bBeingDragged = false;	
  }
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
  center.set(ofGetWidth()/2.0f, ofGetHeight()/2.0f);
  tr_location.setCenter(center);
  tr_location.update();
  monitor.pos_y = ofGetHeight() - monitor.height - monitor.pos_x;
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

// void testApp::guiEvent(ofxUIEventArgs &e)
// {
//   string name = e.widget->getName();
//   int kind = e.widget->getKind();
//   if(name == "SOURCEOLD")
//     {
//       ofxUIDropDownList * ddlist = (ofxUIDropDownList *) e.widget;
//       vector<ofxUIWidget *> &selected = ddlist->getSelected();
//       if (selected.size() == 1){
// 	rosace_input = selected[0]->getName();
// 	cout << "SELECTED: " << selected[0]->getName() << endl;
//       }
//     }
//   else if(name == "camera")
//     {
//       rosace_input = "camera";
//     }
//   else if(name == "mire")
//     {
//       rosace_input = "mire";
//       pixels = images[0].getPixelsRef();
//     }
//   else if(name == "screen")
//     {
//       rosace_input = "screen";
//     }
//   else if(name == "delay")
//     {
//       rosace_input = "lp";
//     }
//   else if (name == "N_ANGLES")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       n_angles = slider->getScaledValue();
//     }
//   else if (name == "RADIUS_1")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       radius_1 = slider->getScaledValue();
//       if (mode_kaleidoscope){tr_location.setTriangleWidth(radius_1);}
//     }
//   else if (name == "RADIUS_2")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       radius_2 = slider->getScaledValue();
//     }
//   else if (name == "RADIUS_3")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       radius_3 = slider->getScaledValue();
//     }
//   else if (name == "RADIUS_4")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       radius_4 = slider->getScaledValue();
//     }
//   else if (name == "DELAY")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       n_delay = slider->getScaledValue();
//       delay.setDelay(n_delay);
//     }
//   else if (name == "N_TRIANGLES")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       n_triangles_k = slider->getScaledValue();
//     }
//   else if (name == "N_LP")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       lp_n_delay = slider->getScaledValue();
//       lp_delay.setNsnum(int(lp_n_delay));
//     }
//   else if (name == "DEC_LP")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       lp_delay.setDelay(slider->getScaledValue());
//     }
//   else if (name == "MAP_B")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       map_b = slider->getScaledValue();
//     }  
// else if (name == "MAP_W")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       map_w = slider->getScaledValue();
//     }
//   else if (name == "white")
//     {
//       lp_mode_black = false;
//     }
//   else if (name == "black")
//     {
//       lp_mode_black = true;
//     }
//   else if (name == "GRAB_ANGLE")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       grab_angle = slider->getScaledValue();
//       step_by_tour = 360.0/grab_angle/sample_lenght;
//     }
//   else if (name == "SAMPLE_LENGHT")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       sample_lenght = slider->getScaledValue();
//       step_by_tour = 360.0/grab_angle/sample_lenght;
//     }        
//   else if (name == "GRAB_1")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       grab_radius_1 = slider->getScaledValue();
//     }  
//   else if (name == "GRAB_2")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       grab_radius_2 = slider->getScaledValue();
//     }  
//   else if (name == "REVOLUTION")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       time_to_turn = slider->getScaledValue() * 1000;
//     }  
//   else if (name == "RADIUS_IN")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       radius_in = slider->getScaledValue();
//       setTexCoord(radius_in, angle_in);
//     }  
//   else if (name == "ANGLE_IN")
//     {
//       ofxUISlider *slider = (ofxUISlider *) e.widget;
//       angle_in = slider->getScaledValue();
//       setTexCoord(radius_in, angle_in);
//     }
//   else if(name == "triangle")
//     {
//       mode_delay_bis = 1;
//     }
//   else if(name == "etoile")
//     {
//       mode_delay_bis = 0;
//     }
//   else if(name == "mandala")
//     {
//       mode_mandala = true;
//       mode_kaleidoscope = false;
//       mode_full = false;
//     }
//   else if(name == "kaleidoscope")
//     {
//       mode_mandala = false;
//       mode_kaleidoscope = true;
//       mode_full = false;
//     }
//   else if(name == "full")
//     {
//       mode_mandala = false;
//       mode_kaleidoscope =false;
//       mode_full = true;
//     }
// }

//--------------------------------------------------------------
void testApp::audioOut(float * output, int bufferSize, int nChannels){
  pan = 0.5f;
  float leftScale = 1 - pan;
  float rightScale = pan;
  
  // sin (n) seems to have trouble when n is very large, so we
  // keep phase in the range of 0-TWO_PI like this:
  

  // ---------------------- noise --------------
  if (mode_audio){
    
    //int t = ofGetElapsedTimeMillis() % time_to_turn; 
    //volume_angle = ofMap(t, 0, time_to_turn, 0, 360);
    //volume_pos.set(center.x + cos(volume_angle*PI/180)*grab_radius_2, 
    // center.y + sin(volume_angle*PI/180)*grab_radius_2);
    //ofColor grab_color_2 = grab.getColor(volume_pos.x, volume_pos.y);
    //volume = ofMap(grab_color_2.r, 0, 254, 0.25, 0.5);
    
    //volume = 0.5;
    float a;
    
    for (int i = 0; i < bufferSize; i++){
      
      if (audio_type==0){	
	ofPoint grab_pos_1;
	ofPoint grab_pos_2;
	a = cur_angle * step_by_tour;
	
	cur_angle = (cur_angle+1) % sample_lenght;
	// if (cur_angle >= sample_lenght){
	// 	cur_angle = 0;
	// }
	
	grab_pos_1.set(center.x + cos(a*PI/180)*grab_radius_1, center.y + sin(a*PI/180)*grab_radius_1);
	grab_pos_2.set(center.x + cos(a*PI/180)*grab_radius_2, center.y + sin(a*PI/180)*grab_radius_2);   
	
	ofColor grab_color_1 = grab.getColor(grab_pos_1.x, grab_pos_1.y);
	ofColor grab_color_2 = grab.getColor(grab_pos_2.x, grab_pos_2.y);
	// m.addIntArg(grab_color.b);
	lAudio[i] = output[i*nChannels    ] = ofMap(grab_color_1.r, 0, 255, -1, 1) * volume * leftScale;
	rAudio[i] = output[i*nChannels +1 ] = ofMap(grab_color_2.b, 0, 255, -1, 1) * volume * leftScale;
	//lAudio[i] = output[i*nChannels    ] = ofRandom(0, 1) * volume * leftScale;
	//rAudio[i] = output[i*nChannels + 1] = ofRandom(0, 1) * volume * rightScale;
      }else{
	if (i <= bufferSize/2){
	  ofPixels  pix_out;
	  output_tex.readToPixels(pix_out);
	  int grab_pos_x = ofMap(i, 0, bufferSize/2, 0, width);
	  ofColor grab_color_1 = pixels.getColor(grab_pos_x, 0);
	  ofColor grab_color_2 = pixels.getColor(grab_pos_x, height-1);

	  lAudio[i] = output[i*nChannels    ] = ofMap(grab_color_1.r, 0, 255, -1, 1) * volume * leftScale;
	  
	  rAudio[i] = output[i*nChannels +1 ] = ofMap(grab_color_2.b, 0, 255, -1, 1) * volume * leftScale;

	  lAudio[bufferSize - i] = output[(bufferSize-i)*nChannels    ] = ofMap(grab_color_1.r, 0, 255, -1, 1) * volume * leftScale;
	  
	  rAudio[bufferSize -i] = output[(bufferSize-i)*nChannels +1 ] = ofMap(grab_color_2.b, 0, 255, -1, 1) * volume * leftScale;
	}

      }
      
    }
  }
  // if(b_recording)
  //   recorder.addAudioSamples(output, bufferSize, nChannels);

}

void testApp::exit() {
    // recorder.close();
}
